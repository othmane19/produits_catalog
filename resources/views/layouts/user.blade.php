<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('css/main2.css')}}">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <title>@yield('title','App store')</title>
</head>
<body>
    <nav class="navbar navbar-expand-sm bg-light navbar-light mb-5 justify-content-end container">
        <ul class="navbar-nav">
            <li class="nav-item"><a class="nav-link" href="{{route('accueil')}}">Accueil</a></li>
            <li class="nav-item"><a class="nav-link" href="{{route('cart.show')}}">Mon panier</a></li>
        </ul>
    </nav>
    <div class="main">
        @yield('content')
    </div>
    <footer>
        &copy;OFPPT 2024
    </footer>
</body>
</html>
