@extends('layouts.user')
 @section('title','Home')
 @section('content')

 <div class="container">
    <div class="row  ">
       @foreach($produits as $p)
       <div class = "col-md-3 mb-4">
       <div class="card row w-100">
           <img class="card-img-top h-100" src={{$p->image}}>
           <div class="card-body h-100">
               <h4 class="card-title">{{$p->designation}}</h4>
               <h4 class="card-text">Prix : {{$p->prix_u}}</h4>
               <form method="POST" action="{{ route('cart.add', $p->id) }}">
                @csrf
                <input type="number" name="quantite" id="quantite" min=1 value=1>
            <input type="submit" class="btn btn-primary" value="Ajouter au panier">
        </form>
           </div>
       </div>
   </div>
@endforeach
 </div>
 @endsection
