@extends('layouts.user')
 @section('title','Home')
 @section('content')
<h1>Shopping cart</h1>
<a href="{{route('cart.clear')}}" class="btn btn-primary">Clear</a>
<h3>Le prix total :</h3>

<table class="table">
    <thead>
        <tr>
            <th>Produit</th>
            <th>Quantite</th>
            <th>prix</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($cartItems as $p)
        <tr>
            <td>{{$p['product']->designation}}</td>
            <td>{{$p['quantity']}}</td>
            <td>{{($p['product']->prix_u)*($p['quantity'])}}</td>
            <td>
                <form method="POST" action="{{ route('cart.destroy', $p['product']->id) }}">
                    @csrf
                <input type="submit" class="btn btn-danger" value="Delete">
            </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

@endsection