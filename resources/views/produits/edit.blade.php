@extends('layouts.admin')
 @section('title','Modifier un produit')
 @section('content')

    <h1>Modifier le produit num {{$p->id}}</h1>
    <form enctype="multipart/form-data" action="{{route('produits.update',["produit"=>$p->id])}}" method="POST">
        @csrf
        @method('PUT')
        <div>
            <label for="image">Select image:</label>
            <input type="file" id="image" name="image" accept="images/*">
        </div>
        <div>
        <label for="designation">Designation</label>
        <input type="text" name="designation" id="designation" value="{{$p->designation}}">
        </div>
        <div>
        <label for="prix_u">Prix</label>
        <input type="number" name="prix_u" id="prix_u" value="{{$p->prix_u}}">
        </div>
        <div>
        <label for="quantite_stock">Quantite en stock</label>
        <input type="number" name="quantite_stock" id="quantite_stock" value="{{$p->quantite_stock}}">
        </div>
         <div>
         <label for="cateforie_id">Categorie</label>
        <select name="categorie_id" id="categorie_id">
            @foreach($categories as $c)
            @if($c->id == $p->categorie->id)
            <option selected="selected" value="{{$c->id}}">{{$c->designation}}</option>
            @else
            <option value="{{$c->id}}">{{$c->designation}}</option>
            @endif
            @endforeach
        </select>
        <div>
            <input type="submit" value="Modifier">
        </div>
    </form>
    <div>
        @if($errors->any())
        <ul>
          @foreach($errors->all() as $er)
           <li>{{$er}}</li>
           
           @endforeach
        </ul>
         


        @endif
    </div>

@endsection
