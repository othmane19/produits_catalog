@extends('layouts.admin')
 @section('title','Gestion des Produits')
 @section('content')
     
    <h1>Liste des Produits</h1>
    <a class="btn btn-primary"href="{{route('produits.create')}}">Ajouter un nouveau produit</a>
    <div class="search container">
      <form action="{{route('produits.index')}}" method="GET">
        <div class="form-group">
        <label for="designation">Designation </label>
      <input type="text" class="form-control" placeholder="Designation" name="designation" id ="designation">
        </div>
        <div class="form-group">
      <label for="prix_min">Prix</label>
      <div class="minmax">
      <input type="number" class="form-control" placeholder="prix min" name="prix_min" id ="prix_min">
      <input type="number" class="form-control" placeholder="prix max" name="prix_max" id ="prix_max">
      </div>    
    </div>
        <div class="form-group">
      <select class="form-control" name="categorie_id" id="categorie_id">
        <option disabled selected value>Selectioner une categorie </option>
        @foreach($categories as $c)
        @if($c->id == $cat)
        <option selected="selected" value="{{$c->id}}">{{$c->designation}}</option>
        @else
        <option value="{{$c->id}}">{{$c->designation}}</option>
        @endif
        @endforeach
      </select>
      <div class="stock form-check">
        <label class="form-check-label" for="quatite_stock">Produits en rupture de stock</label>
        <input class="form-check-input" type="checkbox" id="quantite_stock" name="quantite_stock" value="1">
      </div>
    </div>
    <div class="sa">
      <input type="submit" class="btn btn-primary" value="rechercher">
      <a class="btn btn-success"href="{{route('produits.index',compact('produits'))}}">Afficher tout</a>
    </div> 
    </form>
      </div>
    <table id="tbl">
      <tr>
          <th>Id</th>
        <th>Designation</th>
        <th>Prix_u</th>
        <th>Quantite_stock</th>
        <th>Categorie</th>
        <th colspan="3">Actions</th>
      </tr>
      @foreach ($produits as $p)
          <tr>
            <td>{{$p->id}}</td>
            <td>{{$p->designation}}</td>
            <td>{{$p->prix_u}}</td>
            <td>{{$p->quantite_stock}}</td>
            <td>{{$p->categorie->designation}}</td>
            <td><a class="btn btn-info" href="{{route('produits.show',["produit"=>$p->id])}}">Details</a></td>
            <td><a class="btn btn-info" href="{{route('produits.edit',["produit"=>$p->id])}}">Modifier</a></td>
            <td>
                <form action="{{route('produits.destroy',["produit"=>$p->id])}}" method="POST">
                    @method('DELETE')
                    @csrf
                    <input class="btn btn-danger" type="submit" value="Supprimer" onclick="return confirm('voulez-vous supprimer cette categorie?')">
                </form></td>
          </tr>
      @endforeach
    </table>
    <div class="paginator">
      {!! $produits->links() !!}
  </div>
 @endsection