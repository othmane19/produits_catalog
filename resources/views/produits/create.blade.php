@extends('layouts.admin')
 @section('title','Ajouter un produit')
 @section('content')
    <h1>Creer un nouveau produit</h1>
    <form enctype="multipart/form-data" action="{{route('produits.store')}}" method="POST">
        @csrf
        <div>
            <label for="image">Select image:</label>
            <input type="file" id="image" name="image" accept="images/*">
        </div>
        <div>
        <label for="designation">Designation</label>
        <input type="text" name="designation" id="designation">
        </div>
        <div>
        <label for="prix_u">Prix</label>
        <input type="number" name="prix_u" id="prix_u">
        </div>
        <div>
        <label for="quantite_stock">Quantite en stock</label>
        <input type="number" name="quantite_stock" id="quantite_stock">
        </div>
         <div>
         <label for="cateforie_id">Categorie</label>
        <select name="categorie_id" id="categorie_id">
            @foreach($categories as $c)
            <option value="{{$c->id}}">{{$c->designation}}</option>
            @endforeach
        </select>
        </div>
            <input type="submit" value="Ajouter">
        </div>
    </form>
    <div>
        @if($errors->any())
        <ul>
          @foreach($errors->all() as $er)
           <li>{{$er}}</li>
           
           @endforeach
        </ul>
         


        @endif
    </div>
@endsection