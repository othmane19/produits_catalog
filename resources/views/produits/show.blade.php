@extends('layouts.admin')
 @section('title','Detail d \'un produit')
 @section('content')
    <a href="{{route('produits.index')}}">Retourner vers la liste des produits</a>
    <h1>Detail de la produi Num {{$p->id}}</h1>
    <div>
        <p><strong>Designation:</strong> {{$p->designation}}</p>
        <p><strong>prix unitair:</strong> {{$p->prix_u}}</p>
        <p><strong>Quantite en stock:</strong> {{$p->quantite_stock}}</p>
        <p><strong>Categorie:</strong> {{$p->categorie->designation}}</p>
    </div>
@endsection