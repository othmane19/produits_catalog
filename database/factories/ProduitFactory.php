<?php

namespace Database\Factories;

use App\Models\Categorie;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Produit>
 */
class ProduitFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
        'image'=>'https://istyle.ma/media/catalog/product/cache/image/700x700/e9c3970ab036de70892d86c6d221abfe/a/p/applewatch_s8_gps_starlight_aluminum_starlight_sport_band_pdp_image_position-01_en_1_9.jpg',
        'designation'=>$this->faker->sentence,
        'prix_u'=>$this->faker->randomFloat(2,1,500),
        'quantite_stock'=>$this->faker->randomNumber(3, false),
        'categorie_id'=>Categorie::all()->random()->id
            //
        ];
    }
}
