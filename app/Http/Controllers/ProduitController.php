<?php

namespace App\Http\Controllers;
use App\Models\Produit;
use App\Models\Categorie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class ProduitController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $categories = Categorie::all();
        $q = Produit::query();
        $des = $request->query("designation");
        $cat = $request->query('categorie_id');
        $stock = $request->query('quantite_stock');
        $prix_min = $request->query("prix_min");
        $prix_max = $request->query("prix_max");
        if($des){
            $q->where('designation','like','%'.$des.'%');
        }
        if($prix_min&&$prix_max){
            $q->whereBetween('prix_u',[$prix_min,$prix_max]);
        }
        elseif($prix_min){
            $q->where('prix_u','>',$prix_min);
        }
        elseif($prix_max){
            $q->where('prix_u','<',$prix_max);
        }
        if($cat){
            $q->where('categorie_id','like',$cat);
        }
        if($stock){
            $q->where('quantite_stock','like',0);
        }
        $produits = $q->paginate(15)->appends($request->all());
        return view("produits.index",compact('produits','categories','cat'));
    }
    public function create()
    { 
        $categories = Categorie::all();
        return view('produits.create',compact('categories'));
    }
    public function store(Request $request)
    {
        // $cat=new Categorie();
        // $cat->designation=$request->input('designation');
        // $cat->description=$request->input('description');
        // $cat->save();
        $validatedData = $request->validate([
            'designation'=>'required|unique:produits,designation',
            'prix_u'=>'required|numeric',
            'quantite_stock'=>'required|integer',
            'categorie_id'=>'required',
            'image'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        if($request->hasFile('image')){
            $imagePath = $request->file('image')->store('produits/images','public');
            $validatedData['image'] = $imagePath;
        }
        Produit::create($validatedData);
        return redirect()->route('produits.index');
    }
    public function edit(string $id)
    {
        $categories = Categorie::all();
        $p=Produit::find($id);
        if(is_null($p)){
            abort(404,"Resource non trouve");
        }
        return view('produits.edit',compact('p','categories'));
    }
    public function update(Request $request, string $id)
    {
        $validatedData = $request->validate([
            'designation'=>'required|unique:produits,designation',
            'prix_u'=>'required|numeric',
            'quantite_stock'=>'required|integer',
            'categorie_id'=>'required',
            'image'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        if($request->hasFile('image')){
            $imagePath = $request->file('image')->store('produits/images','public');
            $validatedData['image'] = $imagePath;
        }
        $p=Produit::find($id);
        $p->update($validatedData);
        return redirect()->route('produits.index');
        
    }
    public function show(int $id)
    {
        $p=Produit::find($id);
        
        return view('cart.showp')->with("p",$p);
    }
    /**
     * Show the form for creating a new resource.
     */

}
