<?php

namespace App\Http\Controllers;

use App\Models\Produit;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $produits = Produit::all();
        return view('home',compact('produits'));
    }
    public function addToCart($id,Request $request)
{
    $produit = Produit::find($id);
    $quantite = $request->input("quantite");
    $cartItems = session()->get('cartItems', []);

    $cartItems[] = ['product' => $produit, 'quantity' => $quantite];


    session()->put('cartItems', $cartItems);


    return redirect()->back()->with('success', 'Product added to cart');
}


public function showCart()
{
    $cartItems = session('cartItems', []);


    return view('cart.show', compact('cartItems'));
}
public function destroy(string $id)
{
    
    // Retrieve existing cart items from the session
// Retrieve existing cart items from the session
$cartItems = session()->get('cartItems', []);

// Find the index of the cart item with the given product ID
$itemIndex = null;

foreach ($cartItems as $index => $cartItem) {
    if ($cartItem['product']->id == $id) {
        $itemIndex = $index;
        break;
    }
}

// If the item is found, remove it from the array
if ($itemIndex !== null) {
    unset($cartItems[$itemIndex]);

    // Re-index the array to avoid gaps
    $cartItems = array_values($cartItems);

    // Store the updated cart items back in the session
    session()->put('cartItems', $cartItems);

    return  redirect()->route('cart.show');
}
    
        
    }


public function clear(){
    session()->flush();
    return  redirect()->route('cart.show');
}

}
