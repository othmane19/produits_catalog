<?php

use App\Http\Controllers\CategorieController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProduitController;
use App\Models\Categorie;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/',[HomeController::class,"index"])->name('accueil');
Route::post('/add-to-cart/{id}',[HomeController::class,"addToCart"])->name('cart.add');
Route::post('/delete-cart/{id}',[HomeController::class,'destroy'])->name('cart.destroy');
Route::get('/cart/clear',[HomeController::class,'clear'])->name('cart.clear');
Route::get('/cart',[HomeController::class,"showcart"])->name('cart.show');
Route::get("catergories/search",[CategorieController::class,"search"])->name("categories.search");
Route::resource("categories",CategorieController::class);
Route::resource("produits",ProduitController::class);
